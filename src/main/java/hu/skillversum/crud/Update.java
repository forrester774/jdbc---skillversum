package hu.skillversum.crud;

import java.sql.*;

public class Update {
    public static void main(String[] args){


        Connection connection;

        Statement statement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";


        try{
            connection = DriverManager.getConnection(databaseUrl,user,password);

            statement = connection.createStatement();

            statement.executeUpdate(
                    "update dogs "+ "set owner='Darth Vader'" + "where name='Ubul'");

            resultSet = statement.executeQuery("select * from  dogs order by name");

            while(resultSet.next()){
                System.out.println(resultSet.getString("name")+", " + resultSet.getString("owner"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

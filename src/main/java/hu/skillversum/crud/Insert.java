package hu.skillversum.crud;

import java.sql.*;

public class Insert {
    public static void main(String[] args) {

        Connection connection;

        Statement statement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";

        try{
            connection = DriverManager.getConnection(databaseUrl,user,password);

            statement = connection.createStatement();

            statement.executeUpdate(
                    "insert into dogs "+ "(name, breed, age, owner) " +
                            "values "+
                            "('Ubul', 'Golden Retriever', 1, 'John McClane')");

            resultSet = statement.executeQuery("select * from  dogs order by name");


            while(resultSet.next()){
                System.out.println(resultSet.getString("name")+", " + resultSet.getString("breed"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

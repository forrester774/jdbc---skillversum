package hu.skillversum.crud;

import java.sql.*;

public class Select {
    public static void main(String[] args) {

        Connection connection;

        PreparedStatement preparedStatement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";

        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);

            preparedStatement = connection.prepareStatement("select * from dogs where age > ?");

            preparedStatement.setInt(1, 5);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String breed = resultSet.getString("breed");
                Integer age = resultSet.getInt("age");


                System.out.println(name + " " + breed + " " + " " + age + ".");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

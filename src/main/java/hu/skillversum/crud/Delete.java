package hu.skillversum.crud;

import java.sql.*;

public class Delete {
    public static void main(String[] args) {

        Connection connection;


        Statement statement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";

        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);

            statement = connection.createStatement();

            statement.executeUpdate(
                    "delete from dogs where name='Ubul' and owner='Darth Vader'");

            resultSet = statement.executeQuery("select * from  dogs order by name");

            while (resultSet.next()) {
                System.out.println(resultSet.getString("name") + ", " + resultSet.getString("owner"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

package hu.skillversum.storedProcedures;

import java.sql.*;

public class OutParameters {
    public static void main(String[] args){

        Connection connection;

        CallableStatement statement;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";


        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);

            String breed = "Husky";

            statement = connection.prepareCall("{call number_of_breed(?,?)}");

            statement.setString(1, breed);
            statement.registerOutParameter(2, Types.INTEGER);

            statement.execute();

            int theCount = statement.getInt(2);

            System.out.println("Number of " + breed + "s in the database: " + theCount);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

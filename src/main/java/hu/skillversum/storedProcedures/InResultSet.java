package hu.skillversum.storedProcedures;

import java.sql.*;

public class InResultSet {
    public static void main(String[] args) {


        Connection connection;

        CallableStatement callableStatement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";

        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);

            String owner = "Jane Doe";

            callableStatement = connection.prepareCall("{call get_dogs_for_owner(?)}");


            callableStatement.setString(1, owner);

            callableStatement.execute();

            resultSet = callableStatement.getResultSet();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String theOwner = resultSet.getString("owner");

                System.out.println(name + " " + theOwner);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

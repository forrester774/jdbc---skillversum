package hu.skillversum.storedProcedures;

import java.sql.*;

public class InParameters {
    public static void main(String[] args) {

        Connection connection;

        CallableStatement callableStatement;

        PreparedStatement preparedStatement;

        ResultSet resultSet;

        String databaseUrl = "jdbc:mysql://localhost:3306/dogs";

        String user = "skillversum";
        String password = "password";


        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);

            String breed = "Husky";
            String owner = "Darth Vader";

            callableStatement = connection.prepareCall("{call change_owner_for_breed(?,?)}");

            callableStatement.setString(1, owner);
            callableStatement.setString(2, breed);

            callableStatement.execute();

            preparedStatement = connection.prepareStatement("select * from dogs");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String newOwner = resultSet.getString("owner");

                System.out.println(name + " - " + newOwner);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
